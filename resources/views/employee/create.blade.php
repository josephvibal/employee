@extends('layouts.main')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{URL::route('employee.store')}}" method="POST">
	@csrf
  <div class="form-group">
    <label for="first_name">First Name</label>
    <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="first_name" placeholder="Enter First Name"
    value="{{ old('first_name') }}" 
    >

  </div>
  <div class="form-group">
    <label for="last_name">Last Name</label>
    <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="last_name" placeholder="Enter Last Name"
	value="{{ old('last_name') }}"
    >

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@stop