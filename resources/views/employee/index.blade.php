@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<a href="{{URL::route('employee.create')}}" class="btn btn-primary">Add Employee</a>
	<a href="#" id='refresh' class="btn btn-secondary">Refresh</a>
	</div>
</div>
<div class="row">
	
<div class="col-lg-12">
	<ul class="list-group" id="employeeList">

	</ul>		
</div>

</div>

@stop

@section('js')

<script type="text/javascript">

	function employeeList()
	{
		$.ajax({
		  url: "{{URL::route('employee.getAll')}}",
		  success:function(response) {

	            $.each(response.data, function(i, item) {
	            	console.log(item.first_name);
	            	var full_name = item.first_name + ' ' + item.last_name;
	                $('#employeeList').append("<li>" + full_name + "</li>");
	            });
		   }
		});			
	}

	$(document).ready(function(){
		employeeList();
	});

	$('#refresh').click(function(){
		employeeList();
	});
</script>

@stop